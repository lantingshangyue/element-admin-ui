import {
  createRouter,
  createWebHistory,
  RouteRecordRaw,
  createWebHashHistory,
  createMemoryHistory
} from 'vue-router'
import { watch } from 'vue'
import Home from '../views/Home.vue'
import layout from '../layout/index.vue'
import DynamicRouter from '@/router/DynamicRouter'
import { asideStore } from '@/store/AsideStore'
import { tagsStore } from '@/store/TagsStroe'
import NProgress from 'nprogress'
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home, // () => import('@/views/About.vue'),
    meta: {
      title: '首页',
      keepAlive: false,
      iconClass: 'el-icon-s-home'
    }
  }
]

const list: Array<RouteRecordRaw> = []
function forTree(ll: Array<RouteRecordRaw>) {
  NProgress.start()
  ll.map(item => {
    if (item.children) {
      forTree(item.children)
    } else {
      list.push(item)
    }
  })
}
forTree(DynamicRouter)
export const getRouters = (): Array<RouteRecordRaw> => {
  return [...routes, ...DynamicRouter]
}

const router = createRouter({
  history: createWebHistory(),//(process.env.BASE_URL),
  routes: [...routes, ...list]
})
router.beforeEach((to, from, next) => {
  NProgress.start()
  next()
})

router.afterEach(to => {
 
  asideStore.setCurrentPath(to.path)
  const idx = tagsStore.getState().routers.findIndex(m => m.path === to.path)
  if (idx >= 0) {
    tagsStore.setSelectIndex(idx)
  }
  if (idx < 0) {
    tagsStore.pushRouter(to)
  }
  NProgress.done()
})

export default router
