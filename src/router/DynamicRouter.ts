
import { RouteRecordRaw } from 'vue-router'
import layout from '../layout/components/AppMain.vue'
import Home from '@/views/Home.vue'
import AA from '@/views/AA.vue'
const DynamicRouter: Array<RouteRecordRaw> = [
  {
    path: '/Office',
    name: 'Office',
    component: layout, 
    meta: {
      title: '个人办公',
      keepAlive: true,
      iconClass: 'el-icon-user'
    },
    children:[
      {
        path: '/Office/Todo',
        name: 'Todo',
        component:  () => import('@/views/ee.vue'),
        meta: {
          title: '待办事项',
          keepAlive: true,
          iconClass: 'el-icon-discount'
        },
      },
      {
        path: '/Office/Done',
        name: 'Done',
        component:  () => import('@/views/ee.vue'),
        meta: {
          title: '已办事项',
          keepAlive: true,
          iconClass: 'el-icon-price-tag'
        },
      },
      {
        path: '/Office/Reading',
        name: 'System',
        component:() => import('@/views/ee.vue'),
        meta: {
          title: '待阅事项',
          keepAlive: true,
          iconClass: 'el-icon-reading'
        },
      },
      {
        path: '/Office/Readed',
        name: 'Readed',
        component:  () => import('@/views/ee.vue'),
        meta: {
          title: '已阅事项',
          keepAlive: true,
          iconClass: 'el-icon-collection'
        },
      },
      {
        path: '/Office/Send',
        name: 'Send',
        component:() => import('@/views/ee.vue'),
        meta: {
          title: '我的发起',
          keepAlive: true,
          iconClass: 'el-icon-goods'
        },
      },
      {
        path: '/Office/Share',
        name: 'Share',
        component: () => import('@/views/ee.vue'),
        meta: {
          title: '我的抄送',
          keepAlive: true,
          iconClass: 'el-icon-message'
        },
      }
    ]

  },
  {
    path: '/System',
    name: 'System',
    component: layout, // () => import('@/views/About.vue'),
    meta: {
      title: '系统管理',
      keepAlive: true,
      iconClass: 'el-icon-setting'
    },
    children: [
      {
        path: '/System/Tenant',
        name: 'Tenant',
        component: () => import('@/views/System/Tenant.vue'),
        meta: {
          title: '租户管理',
          keepAlive: true,
          iconClass: 'el-icon-orange'
        }
      }, 
      {
      path: '/System/User',
      name: 'User',
      component: () => import('@/views/System/User.vue'),
      meta: {
        title: '用户管理',
        keepAlive: true,
        iconClass: 'el-icon-user'
      }
    }, {
      path: '/System/Role',
      name: 'Role',
      component: () => import('@/views/System/Role.vue'),
      meta: {
        title: '角色管理',
        keepAlive: true,
        iconClass: 'el-icon-box'
      }
    },
    {
      path: '/System/Menu',
      name: 'Menu',
      component: () => import('@/views/System/Menu.vue'),
      meta: {
        title: '菜单管理',
        keepAlive: true,
        iconClass: 'el-icon-c-scale-to-original'
      }
    },
     {
      path: '/System/Dept',
      name: 'Dept',
      component: () => import('@/views/System/Dept.vue'),
      meta: {
        title: '部门管理',
        keepAlive: true,
        iconClass: 'el-icon-office-building'
      }
    },
    {
      path: '/System/Dic',
      name: 'Dic',
      component: () => import('@/views/System/Dic.vue'),
      meta: {
        title: '字典维护',
        keepAlive: true,
        iconClass: 'el-icon-coin'
      }
    },
    {
      path: '/System/Position',
      name: 'Position',
      component: () => import('@/views/System/Position.vue'),
      meta: {
        title: '职务管理',
        keepAlive: true,
        iconClass: 'el-icon-bank-card'
      }
    },
    ]
  },
  {
    path:"/workflow/",
    name:"workflow",
    component: layout, 
    meta: {
      title: '流程管理',
      keepAlive: true,
      iconClass: 'el-icon-set-up'
    },
    children:[
      {
        path: '/workflow/instance',
        name: 'instance',
        component: () => import('@/views/demo/table.vue'),
        meta: {
          title: '流程实例',
          keepAlive: true,
          iconClass: 'el-icon-coin'
        }
      },
      {
        path: '/workflow/App',
        name: 'App',
        component: () => import('@/views/demo/table.vue'),
        meta: {
          title: '流程应用',
          keepAlive: true,
          iconClass: 'el-icon-mobile'
        }
      },
      {
        path: '/workflow/FlowList',
        name: 'FlowList',
        component: () => import('@/views/demo/table.vue'),
        meta: {
          title: '流程设计',
          keepAlive: true,
          iconClass: 'el-icon-receiving'
        }
      }
    ]
  },
  {
    path:"/demo/",
    name:"Demo",
    component: layout, 
    meta: {
      title: 'demo',
      keepAlive: true,
      iconClass: 'el-icon-user'
    },
    children:[
      {
        path: '/demo/table',
        name: 'Table',
        component: () => import('@/views/demo/table.vue'),
        meta: {
          title: '后台管理表格',
          keepAlive: true,
          iconClass: 'el-icon-user'
        }
      },
      {
        path: '/demo/test',
        name: 'Test',
        component: () => import('@/views/demo/test.vue'),
        meta: {
          title: '测试页',
          keepAlive: true,
          iconClass: 'el-icon-user'
        }
      },
      {
        path: '/demo/Icon',
        name: 'Icon',
        component: () => import('@/views/demo/Icon.vue'),
        meta: {
          title: '图标',
          keepAlive: true,
          iconClass: 'el-icon-user'
        }
      },
    ]

  },


  {
    path: '/about',
    name: 'about',
    component: layout, // () => import('@/views/About.vue'),
    meta: {
      title: '关于页',
      keepAlive: true,
      iconClass: 'el-icon-user'
    },

    children: [
      {
        path: '/about/cc',
        name: 'cc',
        component: () => import('@/views/cc.vue'),
        meta: {
          title: 'cc常规测试页面',
          keepAlive: true,
          iconClass: 'el-icon-user'
        }
      },
      {
        path: '/about/dd',
        name: 'dd',
        component: () => import('@/views/dd.vue'),
        meta: {
          title: 'dd常规测试页面',
          keepAlive: true,
          iconClass: 'el-icon-user'
        }
      },
      {
        path: '/about/ee',
        name: 'ee',
        component: () => import('@/views/ee.vue'),
        meta: {
          title: 'ee常规测试页面不缓存页面',
          keepAlive: true,
          iconClass: 'el-icon-user'
        }
      },
      {
        path: '/demo/ff',
        name: 'ff',
        component: () => import('@/views/ff.vue'),
        meta: {
          title: 'ff常规测试页面不缓存页面',
          keepAlive: true,
          iconClass: 'el-icon-user'
        }
      },
      {
        path: '/demo/gg',
        name: 'gg',
        component: () => import('@/views/gg.vue'),
        meta: {
          title: 'gg常规测试页面不缓存页面',
          keepAlive: true,
          iconClass: 'el-icon-user'
        }
      },
    ]

  }

]
export default DynamicRouter
