import { nextTick, ref, watch } from "vue-demi";

function UseVirtualScroll(
  gridRef: any,
  dataCounts: number,
  showNum: number,
  itemH: number,
  startIndex: any,
  endIndex: any,
  call: any
) {
  let contentHeight: Number = dataCounts * itemH;
  let scrollLeft: number = 0;
  let scrollTop: number = 0;
  let scrollTopLast: number = 0;
  let scrollLeftLast: number = 0;
  let $fixedLeftWrapper: HTMLElement;
  let $fixedRightWrapper: HTMLElement;
  let $fixedLefTable: HTMLElement;
  let $fixedRightTable: HTMLElement;
  let $fixedLeftFixed: HTMLElement;
  let $fixedRighFixed: HTMLElement;

  let $bodyWrapper: HTMLElement;
  let $table: HTMLElement;
  let $space: HTMLElement;

  const resetHeight = () => {
    setTimeout(() => {
      $fixedLeftFixed &&
        ($fixedLeftFixed.style.height =
          parseFloat($fixedLeftFixed.style.height.replace("px", "")) +
          4 +
          "px");
      $fixedRighFixed &&
        ($fixedRighFixed.style.height =
          parseFloat($fixedRighFixed.style.height.replace("px", "")) +
          4 +
          "px");
    }, 1);
  };

  const init = () => {
    $bodyWrapper = gridRef.value.$refs.bodyWrapper;
    $bodyWrapper.scrollTop = scrollTop = scrollTopLast = 0;
    $bodyWrapper.scrollLeft = scrollLeftLast = 0;
    $bodyWrapper.style.position = "relative";
    $bodyWrapper.style.overflow = "scroll";
    //$bodyWrapper.style.scrollBehavior = "smooth";

    $space = $bodyWrapper.querySelector(".space-y") as HTMLElement;
    if ($space) {
      $space.remove();
    }
    $table = $bodyWrapper.querySelector("table.el-table__body") as HTMLElement;
    $table.style.position = "absolute";
    $table.style.left = "0px";
    $table.style.top = "0px";

    $space = document.createElement("div");
    $space.className = "space-y";
    $space.style.width = "0px";
    $space.style.height = contentHeight + "px";
    $bodyWrapper.insertBefore($space, $table);

    //right,left
    const $parentWrapper = $bodyWrapper.parentElement as HTMLElement;
    $fixedLeftWrapper = $parentWrapper.querySelector(
      ".el-table .el-table__fixed .el-table__fixed-body-wrapper"
    ) as HTMLElement;
    $fixedRightWrapper = $parentWrapper.querySelector(
      ".el-table .el-table__fixed-right .el-table__fixed-body-wrapper"
    ) as HTMLElement;

    if ($fixedLeftWrapper) {
      $fixedLeftFixed = $parentWrapper.querySelector(
        ".el-table__fixed "
      ) as HTMLDivElement;

      $fixedLeftWrapper.style.position = "relative";
      $fixedLefTable = $fixedLeftWrapper.querySelector(
        "table.el-table__body"
      ) as HTMLElement;
      $fixedLefTable.style.position = "absolute";
      $fixedLefTable.style.left = "0px";
      $fixedLefTable.style.top = "0px";
      $fixedLeftWrapper.insertBefore($space.cloneNode(), $fixedLefTable);
    }
    if ($fixedRightWrapper) {
      $fixedRighFixed = $parentWrapper.querySelector(
        ".el-table__fixed-right"
      ) as HTMLDivElement;
      $fixedRighFixed.style.height = $fixedRighFixed.clientHeight + 10 + "px";
      $fixedRightWrapper.style.position = "relative";
      $fixedRightTable = $fixedRightWrapper.querySelector(
        "table.el-table__body"
      ) as HTMLElement;
      $fixedRightTable.style.position = "absolute";
      $fixedRightTable.style.right = "0px";
      $fixedRightTable.style.top = "0px";
      $fixedRightWrapper.insertBefore($space.cloneNode(), $fixedRightTable);
    }

    resetHeight();

    let aa = null;
    const scroll = () => {
      scrollTop = $bodyWrapper.scrollTop;
      scrollLeft = $bodyWrapper.scrollLeft;
      $bodyWrapper.setAttribute("x", scrollLeft.toString());
      $bodyWrapper.setAttribute("y", scrollTop.toString());
      //y方向滚动
      //if (scrollTop !== scrollTopLast) {
      scrollTop = $bodyWrapper.scrollTop;
      const idx = Math.floor(scrollTop / itemH);
      const offsetSize = 5;
      const offsetItem = {
        startIndex: Math.max(0, idx - 1 - offsetSize),
        endIndex: idx + showNum + offsetSize,
      };
      if (idx <= startIndex.value || idx >= endIndex.value - showNum - 1) {
        if (
          startIndex.value !== offsetItem.startIndex ||
          endIndex.value !== offsetItem.endIndex
        ) {
          (startIndex.value = offsetItem.startIndex),
            (endIndex.value = offsetItem.endIndex);
          call({ startIndex: startIndex.value, endIndex: endIndex.value });
          nextTick(() => {
            $table.style.marginTop = startIndex.value * itemH + "px";
            $fixedRightTable &&
              ($fixedRightTable.style.marginTop =
                startIndex.value * itemH + "px");
            $fixedLefTable &&
              ($fixedLefTable.style.marginTop =
                startIndex.value * itemH + "px");

            scrollTopLast = scrollTopLast = $bodyWrapper.scrollTop;
            scrollLeftLast = scrollLeft = $bodyWrapper.scrollLeft;
          });
        }
      }
      //}
      if (scrollLeft !== scrollLeftLast) {
        //x方向滚动
        scrollTopLast = scrollTopLast = $bodyWrapper.scrollTop;
        scrollLeftLast = scrollLeft = $bodyWrapper.scrollLeft;
      }

      resetHeight();
    };

    $bodyWrapper.addEventListener("wheel", function (e) {
      console.log(+e.deltaX + "," + e.deltaY);
      console.log(e);
      scrollTop = $bodyWrapper.scrollTop;
      scrollLeft = $bodyWrapper.scrollLeft;

      if (scrollLeft !== scrollLeftLast) {
        //X方向
        //$bodyWrapper.scrollLeft=e.deltaX
        nextTick(() => {
          scrollTopLast = scrollTop = $bodyWrapper.scrollTop;
          scrollLeftLast = scrollLeft = $bodyWrapper.scrollLeft;
        });
      }

      //y方向
      if (scrollTop !== scrollTopLast) {
        const deltaY = e.deltaY;
        const deltaTop = deltaY; //browse.firefox ? deltaY * 40 : deltaY
        const isTopWheel = deltaTop < 0;
        const scrollBodyElem = $bodyWrapper;
        // 如果滚动位置已经是顶部或底部，则不需要触发
        if (
          isTopWheel
            ? scrollBodyElem.scrollTop <= 0
            : scrollBodyElem.scrollTop >=
              scrollBodyElem.scrollHeight - scrollBodyElem.clientHeight
        ) {
          return;
        }

        aa = setTimeout(() => {
          e.preventDefault();
          clearTimeout(aa);
          $bodyWrapper.scrollTop =
            $bodyWrapper.scrollTop + Math.abs(deltaY) * (isTopWheel ? -1 : 1);
        }, 50);
        scrollTopLast = scrollTop = $bodyWrapper.scrollTop;
        scrollLeftLast = scrollLeft = $bodyWrapper.scrollLeft;
      }
    });
    $bodyWrapper.addEventListener("scroll", scroll);
  };
  init();
  return {
    init,
  };
}
export { UseVirtualScroll };
