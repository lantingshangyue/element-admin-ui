import { Store } from '@/store/Store'
export interface IToolBarItem {
  text: string
  icon: string
  type: string
  hide:any|boolean,
  actions:string,
  fn:any,
  role:string
}
export interface ToolBarState{
  list:Array<IToolBarItem>
}

export class ToolBarStore extends Store<ToolBarState> {
  protected data(): ToolBarState {
     return {
       list: [],
     }
  }

  setList(list:Array<IToolBarItem>):void {
    this.state.list = list
  }

  addList(item:IToolBarItem) :void{
    this.state.list.push(item)
  }

  hide(action:string) :void {
   const item = this.state.list.find(m => m.actions === action)
    if (item) {
      item.hide = true
    }
  }

  show(action:string):void{
    const item = this.state.list.find(m => m.actions === action)
    if (item) {
      item.hide = false
    }
  }

  resetFn(action:string, fn:()=>void):void {
    const item = this.state.list.find(m => m.actions === action)
    if (item) {
      item.fn = fn
    }
  }
}
