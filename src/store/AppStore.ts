
import { Store } from './Store'

export enum ClientType {
  mobile = 'mobile',
  pc = 'pc',
  ipad = 'ipad'
}
export enum LayoutType {
  split = 'split', //分栏 将菜单纵向布局分类 to do
  y = 'y', //纵向布局  菜单纵向布局 
  x = 'x', //横向布局 菜单横向布局  to do
}

export interface AppState extends Object {

  name: string,
  /** 布局类型 */
  layoutType: LayoutType,
  /** 客户端类型 */
  clientType: ClientType, //mobile,pc,ipad
  /** 客户端宽度 */
  width: number, // 客户端宽度
  /** 客户端高度 */
  height: number//高度
  /** 菜单折叠状态 */
  collapseState: boolean
}

export class AppStore extends Store<AppState> {
  protected data(): AppState {
    return {
      layoutType: LayoutType.y,
      clientType: ClientType.pc,
      name: '',
      width: 1200,
      height: 100,
      collapseState: true
    }
  }

  public setCollapseState(state: boolean): void {
    this.state.collapseState = state
  }

  public setClientType(type: ClientType): void {
    this.state.clientType = type
  }

  public setWidth(w: number): void {
    this.state.width = w
  }

  public setHeight(h: number): void {
    this.state.height = h
  }
}

export const appStore: AppStore = new AppStore()
