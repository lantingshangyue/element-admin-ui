import { appStore, ClientType } from '@/store/AppStore'
import { asideStore } from '@/store/AsideStore'
import { tagsStore } from '@/store/TagsStroe'

export {
  appStore,
  tagsStore,
  asideStore,
}
