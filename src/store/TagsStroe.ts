import { Store } from './Store'
import { RouteRecordRaw, useRouter, useRoute, RouteLocationNormalizedLoaded } from 'vue-router'
import { nextTick, watch } from 'vue'

export interface TagsState {
  routers: Array<RouteRecordRaw> | Array<any>,
  isOverflow: boolean,
  selectIndex: number,
  hoverIndex: number
}

export class TagsStore extends Store<TagsState> {
  /** 状态初始化*/
  protected data(): TagsState {
    return {
      routers: [
        { name: 'Home', path: '/', meta: { title: '首页', keepAlive: false, iconClass: 'el-icon-s-home' } },
        // { name: 'Table', path: '/demo/table', meta: { title: 'table页', keepAlive: true, iconClass: 'el-icon-user' } },
        // { name: 'Test', path: '/demo/test', meta: { title: '测试页', keepAlive: true, iconClass: 'el-icon-user' } }
      ],
      isOverflow: false,
      selectIndex: -1,
      hoverIndex: -1
    }
  }

  public setRouters(routers: RouteRecordRaw[]): void {
    this.state.routers = routers
  }

  public setSelectIndex(idx: number): void {
    this.state.selectIndex = idx
  }

  /** 添加tab */
  public pushRouter(recordRow: RouteRecordRaw | any): void {
    this.state.routers.push(recordRow)
    this.state.selectIndex = this.state.routers.length - 1
  }

  /** 移除tab */
  public removeRouter(recordRow: RouteRecordRaw): void {
    const index = this.state.routers.findIndex(m => m.path === recordRow.path)
    const selRow = this.state.routers[this.state.selectIndex]
    this.state.routers.splice(index, 1)
    const selIndex = this.state.routers.findIndex(m => m.path === selRow.path)
    const a = Number((selIndex < 0 ? this.state.routers.length - 1 : selIndex))
    this.state.selectIndex = a
  }

  /** 跳转tab*/
  public linkRouterSS(item: RouteRecordRaw): void {
    /*
    const index = this.state.routers.findIndex(m => m.path === item.path)
    this.state.selectIndex = index < 0 ? 0 : index
    */

  }

  /** 移出所有，除首页*/
  public closeAll(): void {
    this.state.routers.splice(1)
    this.state.selectIndex = 0
  }

  //关闭其他标签
  public closeOther(): void {
    const selRow = this.state.routers[this.state.selectIndex]
    for (let i = this.state.routers.length - 1; i >= 1; i--) {
      if (this.state.selectIndex !== i) {
        this.state.routers.splice(i, 1)
      }
    }
    const selIndex = this.state.routers.findIndex(m => m.path === selRow.path)
    this.state.selectIndex = selIndex < 0 ? 0 : selIndex
  }

  /** 移出右边标签*/
  public closeRight(): void {
    const selRow = this.state.routers[this.state.selectIndex]
    for (let i = this.state.routers.length - 1; i >= 1; i--) {
      if (this.state.selectIndex < i) {
        this.state.routers.splice(i, 1)
      }
    }
    const selIndex = this.state.routers.findIndex(m => m.path === selRow.path)
    this.state.selectIndex = selIndex < 0 ? 0 : selIndex
  }

  /** 移出左边标签 */
  public closeLeft(): void {
    const selRow = this.state.routers[this.state.selectIndex]
    for (let i = this.state.routers.length - 1; i >= 1; i--) {
      if (this.state.selectIndex > i) {
        this.state.routers.splice(i, 1)
      }
    }
    const selIndex = this.state.routers.findIndex(m => m.path === selRow.path)
    this.state.selectIndex = selIndex < 0 ? 0 : selIndex
  }
}
export const tagsStore: TagsStore = new TagsStore()
