import { Store } from './Store'
import { appStore, ClientType } from './AppStore'
import { RouteRecordRaw } from 'vue-router'

export interface AsideState extends Object {
  menuList: Array<RouteRecordRaw>,
  /**侧边栏展开状态 */
  collapse: boolean,
  /** 侧边栏总宽度 */
  width: number,
  /** 是否手机端 打开了侧边菜单 */
  isMobileOpen: boolean
  /** 当前菜单路径*/
  currentPath: string
}
export class AsideStore extends Store<AsideState> {
  protected data(): AsideState {
    const data = {
      menuList: [
       
      ],
      collapse: false,
      width: 200,
      isMobileOpen: false,
      currentPath: ''
    }
    return data
  }

  public setCurrentPath(currentPath: string): void {
    this.state.currentPath = currentPath
  }

  public setCollapse(state: boolean): void {
    this.state.collapse = state
    if (appStore.getState().clientType === ClientType.pc && this.state.collapse === false) {
      this.state.width = 200
    } else if (appStore.getState().clientType === ClientType.pc && this.state.collapse === true) {
      this.state.width = 65
    } else if (appStore.getState().clientType === ClientType.ipad && this.state.collapse === false) {
      this.state.width = 200
    } else if (appStore.getState().clientType === ClientType.ipad && this.state.collapse === true) {
      this.state.width = 65
    } else if (appStore.getState().clientType === ClientType.mobile && this.state.collapse === false) {
      this.state.width = 200
      this.state.isMobileOpen = true
    } else if (appStore.getState().clientType === ClientType.mobile && this.state.collapse === true) {
      this.state.width = 0
      this.state.isMobileOpen = false
    }
  }
}
export const asideStore: AsideStore = new AsideStore()
