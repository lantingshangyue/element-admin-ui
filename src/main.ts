import { createApp, defineComponent, inject } from 'vue'
import App from './App.vue'
import router from './router'
import ElementPlus from 'element-plus'
import locale from 'element-plus/lib/locale/lang/zh-cn'
import 'element-plus/dist/index.css'
import '@/styles/ui.scss'
import 'nprogress/nprogress.css'
import 'font-awesome/css/font-awesome.min.css'
import { appStore, ClientType } from '@/store/AppStore'
import  Watermark from '@/utils/Watermark'

//import draggable from 'vuedraggable'
 
const app = createApp(App)
app.use(ElementPlus, { size: 'small', locale })//mini
  .use(router, { router })
 // .use(draggable)
  .mount('#app')
 
 
  Watermark('admin','管理员','13888888888')
 
// 注册
app.directive('loadmore', {
  mounted(el, binding) {
    //const xtable: any = inject('xtable')
   // const tableState = inject('tableState') as any

    const selectWrap = el.querySelector('.el-table__body-wrapper')
    selectWrap.addEventListener('scroll', function (e) {
      const sign = 100
      const that = e.target
      const scrollDistance = that.scrollHeight - that.scrollTop - that.clientHeight
      let isScroll = false 
      if (scrollDistance <= sign && !isScroll) {
        isScroll = true
        binding.value()
        isScroll = false
      }
    })
  }

})

// 注册
app.directive('fit', {
  mounted(el:HTMLElement, binding) {
      el.style.height = '100%'
      el.style.width = '100%'
      if (appStore.getState().clientType === ClientType.mobile) {
        el.style.overflowY = 'scroll'
        el.style.overflowX = 'hidden'
      } else {
        el.style.overflow = binding.value ? 'hidden' : 'scroll'
      }
  }
})

app.directive('focus', {
  mounted(el:HTMLElement, binding) {
    debugger
    setTimeout(() => {
      if (el.tagName === 'INPUT') { el.focus() } else {
        const INPUT = el.querySelector('input') as HTMLElement
        INPUT && INPUT.focus()
      }
      }, 10)
   }
})


 


 
      

       
        