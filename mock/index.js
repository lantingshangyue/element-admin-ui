const { Random } = require('mockjs')
const Mock=require('mockjs')

function pagination(pageNo, pageSize, array) {
  var offset = (pageNo - 1) * pageSize;
  return (offset + pageSize >= array.length) ? array.slice(offset, array.length) : array.slice(offset, offset + pageSize);
  }


const proxy=[
  {
    url:"/api/getpagelist",
    method:"POST",
    response:(req)=>{
      var obj={
        "list|200":[{
          "id|+1":1,
          "name":"@cname(3)@id",
          "hobby|1":["选项1","选项2"],
          "sex|1":["女","男"],
          "age|15-35": 15,
          "adult": 200333,
          "minor|1": ['是','否'],
          "address": "@county(true)",
          "position|1": ["总经理","助理","程序员","财务","行政经理","市场专员"],
          "birth": Random.date("yyyy-MM-dd")
        }]
      }
      for(var i=1;i<=100;i++){
        obj["list|200"][0]["col"+i]="@cname(3)";
      }
      var data=Mock.mock(obj)
      return {
        status: 'ok',
        code: '',
        items: pagination(req.body.pageIndex,req.body.pageSize, data.list),
        total: data.list.length
      }
    }

  },
  {
    url:"/api/getheaders",
    method:"post",
    response:()=>{
      var items=[
        //  { type: 'expand', prop: 'expand', label: '展开' },

       { type: 'index', prop: 'index', label: '序号' },
         { type: 'selection', prop: 'selection', label: '序号' },
     { width: 45, prop: 'id', label: '操作', align: 'center', fixed: 'none' },
       
          {
            width: 120,
            label: '姓名',
            prop: 'name',
            sortable: 'custom',
             align: 'center',
            columnKey: 'name',
            search: true,
            editor: {
              type: 'text',
              blur: null, //row,col
              focus: null,
              change: null,
              options: {
                rules: {
                  name: [{ type: 'string', required: true, message: '输入错误' }]
                }
              }
            },
            type: 'string', //int bigint,decimal,guid,bool
            len: 9,
            decimal: 2,
            required: true,
            isHide:false,//
            iconClass:'fa fa-vcard-o',
            iconTipClass:" el-icon-info"
          },
          {
            width: 120,
            label: '职位',
            prop: 'position',
            sortable: 'custom',
            align: 'center',
            columnKey: 'position',
            search: true,
            editor: {
              type: 'text',
              blur: null, //row,col
              focus: null,
              change: null,
              options: {
                rules: {
                  position: [{ type: 'string', required: true, message: '输入错误' }]
                }
              }
            },
            type: 'string', //int bigint,decimal,guid,bool
            len: 9,
            decimal: 2,
            required: true,
            iconClass:'fa fa-drivers-license'
          },
          {
            width:180,
            label: '生日',
            prop: 'birth',
            sortable: 'custom',
             align: 'center',
            columnKey: 'name',
            search: true,
            editor: {
              type: 'date',
              blur: null, //row,col
              focus: null,
              change: null,
              options: {
                rules: {
                  name: [{ type: 'string', required: true, message: 'aa输入错误' }]
                }
              }
            },
            type: 'string', //int bigint,decimal,guid,bool
            len: 9,
            decimal: 2,
            required: true,
            iconClass:"fa fa-birthday-cake"
          },
          {
            width:300,
            label: '地址',
            prop: 'address',
            sortable: 'custom',
             align: 'center',
            columnKey: 'name',
            search: true,
            editor: {
              type: 'text',
              blur: null, //row,col
              focus: null,
              change: null,
              options: {
                rules: {
                  name: [{ type: 'string', required: true, message: 'aa输入错误' }]
                }
              }
            },
            iconClass:"fa fa-address-book"
          },
          {
            width: 200,
            label: '爱好',
            prop: 'hobby', 
            align: 'center',
            sortable: 'custom',
            search: true,
            editor: {
              type: 'select',
              options: {
                rules: {
                  hobby: [{ type: 'string', required: true, message: '不能为空' }]
                },
                data: [
                  { value: '1', text: '选项1' },
                  { value: '2', text: '选项2' }
                ],
                valueField: 'value',
                textField: 'text'
              }
            },
            type: 'string', //int bigint,decimal,guid
            len: 9,
            decimal: 2,
            required: true,
            configKey: 'user' ,
            iconClass:"fa fa-mortar-board"

          },
          
         {
            label: '基本情况',
            align: 'center',
            children: [
              { width: 200, label: '性别', align: 'center', prop: 'sex', search: true, sortable: 'custom', iconClass:"fa fa-intersex" ,
              editor:{
                type: 'select', 
                options:{
                  data:[{text:'男',value:'男'},{text:'女',value:'女'}],
                  rules:{
                    sex: [ {type:"string" , required: true, message: '不能为空'}  ]
                  }
                }
              }
            },
              {
              width: 200,
              label: '年龄',
              align: 'center',
              prop: 'age',
              search: true,
              len: 9,
              decimal: 2,
              required: true,
              type:"int",
              editor:{
                options:{
                  rules:{
                    age: [ {type:"number" , required: true, message: '不能为空'}  ]
                  }
                }
              }
             },
              {
                width: 200,
                label: '是否成年',
                children: [
                  { width: 200, label: '成年', prop: 'adult', align: 'center' },
                  { width: 200, label: '未成年', prop: 'minor', align: 'center' }
                ]
              }
            ]
          },
          
        ]

        for(var i=1;i<=2;i++){
          items.push(  {
            width: 120,
            label: '大表测试列'+i,
            prop: 'col'+i,
            fixed:"none",
            sortable: 'custom',
             align: 'center',
            columnKey: 'col'+i,
            search: false,
            editor: {
              type: 'text',
              blur: null, //row,col
              focus: null,
              change: null,
              options: {
                rules: {
                  name: [{ type: 'string', required: true, message: '输入错误' }]
                }
              }
            },
            type: 'string', //int bigint,decimal,guid,bool
            len: 9,
            decimal: 2,
            required: true,
            isHide:false//
          })
        }
      return  {
        status: 'ok',
        code: '',
        items: items
      }
    }

  }
]
module.exports = proxy
