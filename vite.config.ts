import { defineConfig, UserConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import { viteMockServe } from "vite-plugin-mock";
export default defineConfig({

 server:{
    port:3001,
    open:false,
    https:true,
    host:"0.0.0.0",
 },
  optimizeDeps:{
    include:[]
  },
  plugins: [vue(),
    viteMockServe({
      mockPath:"mock",
      supportTs: false,
      localEnabled:true
  })
  ],
  resolve:{
    alias:{
      "@":path.resolve(__dirname,"src")
    }
  },
  define:{
    'process.env':{
      NODE_ENV:'development'
    }
  }
})
